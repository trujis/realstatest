package com.trujis.realstatest

import android.app.Application
import com.trujis.common.di.DaggerCommonComponent
import com.trujis.common.utils.Logger
import com.trujis.users.di.DaggerUsersTestComponent
import com.trujis.users.di.UsersTestComponent
import com.trujis.users.di.provider.UsersTestInjectorProvider

class UserTestApplication : Application(), UsersTestInjectorProvider {

    private val commonComponent by lazy { DaggerCommonComponent.factory().create(this) }

    override fun usersTestInjector(): UsersTestComponent {
        return DaggerUsersTestComponent.factory().create(commonComponent, this)
    }

    override fun onCreate() {
        super.onCreate()
        onLaunchActions()
    }

    private fun onLaunchActions() {
        Logger.initialize((BuildConfig.DEBUG))
    }
}