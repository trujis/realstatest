package com.trujis.common.utils

import android.content.Context
import android.os.IBinder
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.*

class DateUtilsTest {


    @Before
    fun before() {

    }

    @Test
    fun shouldNotParseWhenInputIsEmpty() {
        //Act
        val dateResult = "".toDateFormatted()

        //Assert
        Assert.assertNull(dateResult)
    }

    @Test
    fun shouldNotParseWhenInputIsInvalid() {
        //Act
        val dateResult = "Hello World".toDateFormatted()

        //Assert
        Assert.assertNull(dateResult)
    }

    @Test
    fun shouldAddHoursCorrectlyWhenRequested() {
        //Act
        val dateResult = VALID_DATE_TIME.toDateFormatted()

        //Assert
        Assert.assertEquals("20/04/2013", dateResult)
    }

    @Test
    fun shouldParseCorrectlyGivenACustomOutputFormat() {
        //Act
        val dateResult = VALID_DATE_TIME.toDateFormatted("yyyy-MM-dd")

        //Assert
        Assert.assertEquals("2013-04-20", dateResult)
    }

    @Test
    fun shouldParseDateCorrectlyGivenACustomOutputFormat() {
        //Act
        val dateResult = VALID_DATE.toDateFormatted("yyyy/MM/dd", "yyyy-MM-dd")

        //Assert
        Assert.assertEquals("2013/04/20", dateResult)
    }

    companion object {
        const val VALID_DATE_TIME = "2013-04-20T01:55:27.640Z"
        const val VALID_DATE = "2013-04-20"
    }
}