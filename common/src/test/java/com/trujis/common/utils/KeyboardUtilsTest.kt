package com.trujis.common.utils

import android.content.Context
import android.os.IBinder
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class KeyboardUtilsTest {

    @Mock
    lateinit var event: KeyEvent
    @Mock
    lateinit var view: View
    @Mock
    lateinit var context: Context
    @Mock
    lateinit var inputMethodManager: InputMethodManager
    @Mock
    lateinit var windowToken: IBinder

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)

        Mockito.`when`(context.getSystemService(Context.INPUT_METHOD_SERVICE)).thenReturn(inputMethodManager)
        Mockito.`when`(view.context).thenReturn(context)
        Mockito.`when`(view.windowToken).thenReturn(windowToken)
    }

    @Test
    fun shouldReturnTrueWhenActionIsDownAndyKeyCodeIsEnter() {
        //Arrange
        Mockito.`when`(event.action).thenReturn(KeyEvent.ACTION_UP)

        //Act
        val isEnterClicked = KeyboardUtils.isEnterClicked(KeyEvent.KEYCODE_ENTER, event)

        //Assert
        Assert.assertEquals(true, isEnterClicked)
    }

    @Test
    fun shouldNotReturnTrueWhenActionIsNotDownAndyKeyCodeIsEnter() {
        //Arrange
        Mockito.`when`(event.action).thenReturn(KeyEvent.ACTION_DOWN)

        //Act
        val isEnterClicked = KeyboardUtils.isEnterClicked(KeyEvent.KEYCODE_ENTER, event)

        //Assert
        Assert.assertEquals(false, isEnterClicked)
    }

    @Test
    fun shouldNotReturnTrueWhenActionIsDownAndyKeyCodeIsNotEnter() {
        //Arrange
        Mockito.`when`(event.action).thenReturn(KeyEvent.ACTION_UP)

        //Act
        val isEnterClicked = KeyboardUtils.isEnterClicked(KeyEvent.KEYCODE_11, event)

        //Assert
        Assert.assertEquals(false, isEnterClicked)
    }

    @Test
    fun shouldDoNothingWhenHidingKeyboardGivenAnInvalidContext() {
        //Arrange
        Mockito.`when`(view.context).thenReturn(null)

        //Act
        KeyboardUtils.hideKeyboard(view)

        //Assert
        Mockito.verify(inputMethodManager, Mockito.times(0))
            .hideSoftInputFromWindow(ArgumentMatchers.any(), ArgumentMatchers.anyInt())
    }

    @Test
    fun shouldHideKeyboardWhenHidingKeyboardGivenAValidData() {
        //Act
        KeyboardUtils.hideKeyboard(view)

        //Assert
        Mockito.verify(inputMethodManager, Mockito.times(1)).hideSoftInputFromWindow(windowToken, 0)
    }

}