package com.trujis.common.di.scope

import javax.inject.Scope

@Scope
@Retention
annotation class FeatureScope