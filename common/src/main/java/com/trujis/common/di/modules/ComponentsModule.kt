package com.trujis.common.di.modules

import com.squareup.picasso.Picasso
import com.trujis.common.BuildConfig
import com.trujis.common.ui.utils.ImageLoader
import com.trujis.common.ui.utils.ImageLoaderImpl
import dagger.Module
import dagger.Provides

@Module
class ComponentsModule {

    @Provides
    fun getPicasso(): Picasso {
        val picasso = Picasso.get()
        picasso.setIndicatorsEnabled(BuildConfig.DEBUG)
        return picasso
    }

    @Provides
    fun provideImageLoader(picasso: Picasso): ImageLoader {
        return ImageLoaderImpl(picasso)
    }

}