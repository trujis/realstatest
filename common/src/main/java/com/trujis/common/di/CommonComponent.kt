package com.trujis.common.di

import android.app.Application
import android.content.Context
import com.squareup.picasso.Picasso
import com.trujis.common.di.modules.ComponentsModule
import com.trujis.common.di.modules.NetworkModule
import com.trujis.common.ui.utils.ImageLoader
import dagger.BindsInstance
import dagger.Component
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class, ComponentsModule::class])
interface CommonComponent {

    fun okHttpClient(): OkHttpClient
    fun imageLoader(): ImageLoader
    fun picasso(): Picasso

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance applicationContext: Context): CommonComponent
    }

    fun inject(application: Application)

}