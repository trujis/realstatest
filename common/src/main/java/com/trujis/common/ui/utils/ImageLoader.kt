package com.trujis.common.ui.utils

import android.widget.ImageView
import com.trujis.common.R

interface ImageLoader {

    /**
     * Adds URL image into imageView component using default placeHolder and onError images
     */
    fun loadInto(imageView: ImageView, url: String, placeHolder: Int = R.drawable.ic_abstract_user_flat, onError: Int = R.drawable.ic_abstract_user_flat)

    fun setPolicy(policy: ImagePolicy): ImageLoader

}
