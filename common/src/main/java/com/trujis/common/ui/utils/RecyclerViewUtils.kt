package com.trujis.common.ui.utils

import android.os.Handler
import android.os.Looper
import androidx.recyclerview.widget.RecyclerView

object RecyclerViewUtils {

    fun RecyclerView.scrollDelayedToPosition(position: Int) {
        Handler(Looper.getMainLooper()).postDelayed({
            scrollToPosition(position)
        }, 200)
    }

}