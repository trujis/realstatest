package com.trujis.common.ui.utils

import android.os.Handler
import android.os.Looper
import android.text.Editable
import androidx.core.widget.addTextChangedListener
import com.google.android.material.textfield.TextInputEditText

object TextInputEditTextUtils {
    fun TextInputEditText.addDelayedTextChangedListener(action: (Editable?) -> Unit, delay: Long? = null) {
        val finalDelay = delay ?: DEFAULT_DELAYED_TEXT_INPUT_LISTENER
        val handler = Handler(Looper.getMainLooper())
        val runnable = EditableRunnable(action)

        addTextChangedListener { editable ->
            handler.removeCallbacks(runnable)
            runnable.editable = editable
            handler.postDelayed(runnable, finalDelay)
        }
    }

    const val DEFAULT_DELAYED_TEXT_INPUT_LISTENER = 1000L
}

private class EditableRunnable(private val action: (Editable?) -> Unit): Runnable {

    var editable: Editable? = null

    override fun run() {
        action.invoke(editable)
    }

}