package com.trujis.common.ui.utils

import android.widget.ImageView
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import javax.inject.Inject

class ImageLoaderImpl @Inject constructor(
    private val picasso: Picasso
) : ImageLoader {

    private var policy: ImagePolicy? = null

    override fun loadInto(imageView: ImageView, url: String, placeHolder: Int, onError: Int) {
        picasso.load(url)?.apply {
            if (placeHolder > 0)
                placeholder(placeHolder)
            if (onError > 0)
                error(onError)
            getPolicy()?.let { networkPolicy(it) }
            fit()
            into(imageView)
            policy = null
        }
    }

    override fun setPolicy(policy: ImagePolicy): ImageLoader {
        this.policy = policy
        return this
    }

    private fun getPolicy(): NetworkPolicy? {
        return when (policy) {
            ImagePolicy.NO_CACHE -> NetworkPolicy.NO_CACHE
            ImagePolicy.NO_STORE -> NetworkPolicy.NO_STORE
            ImagePolicy.OFFLINE -> NetworkPolicy.OFFLINE
            null -> null
        }
    }

}

enum class ImagePolicy {
    NO_CACHE,
    NO_STORE,
    OFFLINE
}