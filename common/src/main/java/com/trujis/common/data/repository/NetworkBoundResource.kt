package com.trujis.common.data.repository

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.trujis.common.data.Resource
import com.trujis.common.data.api.ApiEmptyResponse
import com.trujis.common.data.api.ApiErrorResponse
import com.trujis.common.data.api.ApiResponse
import com.trujis.common.data.api.ApiSuccessResponse
import com.trujis.common.utils.AppExecutors
import timber.log.Timber

/**
 * A generic class that can provide a resource backed by both the sqlite database and the network.
 *
 *
 * You can read more about it in the [Architecture
 * Guide](https://developer.android.com/arch).
 * @param <ResultType>
 * @param <RequestType>
</RequestType></ResultType> */
abstract class NetworkBoundResource<ResultType, RequestType>
@MainThread constructor(
    private val appExecutors: AppExecutors
) {

    companion object {
        private const val TAG = "NetworkBoundResource"
    }

    private val result = MediatorLiveData<Resource<ResultType>>()

    init {
        result.value = Resource.loading(null)
        @Suppress("LeakingThis")
        val dbSource: LiveData<ResultType>? = loadFromDb()
        dbSource?.let {
            result.addSource(dbSource) { data ->
                Timber.i("LiveData loaded from DB")
                result.removeSource(dbSource)
                if (shouldFetch(data)) {
                    fetchFromNetwork(dbSource)
                } else {
                    result.addSource(dbSource) { newData ->
                        setValue(Resource.success(newData))
                    }
                }
            }
        } ?: run {
            fetchFromNetwork(null)
        }
    }

    @MainThread
    private fun setValue(newValue: Resource<ResultType>) {
        if (result.value != newValue) {
            result.value = newValue
        }
    }

    private fun fetchFromNetwork(dbSource: LiveData<ResultType>?) {
        Timber.i("Fetching from Network...")
        val apiResponse = createCall()

        dbSource?.let {
            result.addSource(dbSource) { newData ->
                setValue(Resource.loading(newData))
            }
        }

        result.addSource(apiResponse) { response ->
            result.removeSource(apiResponse)

            dbSource?.let {
                result.removeSource(dbSource)
            }

            when (response) {
                is ApiSuccessResponse -> {
                    Timber.i("Network success.")
                    appExecutors.diskIO().execute {
                        val responseBody = processResponse(response)
                        Timber.i("   Saving data into DB")
                        saveCallResult(responseBody)
                        appExecutors.mainThread().execute {
                            // we specially request a new live data,
                            // otherwise we will get immediately last cached value,
                            // which may not be updated with latest results received from network.
                            dbSource?.let { source ->
                                result.addSource(source) { newData ->
                                    Timber.i("   LiveData loaded from DB after network")
                                    setValue(Resource.success(newData))
                                }
                            } ?: run {
                                setValue(Resource.success(responseBody) as Resource<ResultType>)
                            }

                        }
                    }
                }
                is ApiEmptyResponse -> {
                    Timber.i("Empty Data from Network.")
                    appExecutors.mainThread().execute {
                        dbSource?.let {
                            result.addSource(dbSource) { newData ->
                                Timber.i("   LiveData loaded from previous DB results")
                                setValue(Resource.success(newData))
                            }
                        } ?: run {
                            setValue(Resource.success(null))
                        }
                    }
                }
                is ApiErrorResponse -> {
                    Timber.e("Error from Network. ${response.errorMessage}")
                    onFetchFailed()

                    appExecutors.mainThread().execute {
                        dbSource?.let {
                            result.addSource(dbSource) { newData ->
                                Timber.i("   LiveData loaded from previous DB results")
                                setValue(Resource.error(response.errorMessage, newData))
                            }
                        } ?: run {
                            setValue(Resource.error(response.errorMessage, null))
                        }
                    }
                }
            }
        }
    }

    protected open fun onFetchFailed() {}

    fun asLiveData() = result as LiveData<Resource<ResultType>>

    @WorkerThread
    protected open fun processResponse(response: ApiSuccessResponse<RequestType>) = response.body

    @WorkerThread
    protected abstract fun saveCallResult(item: RequestType)

    @MainThread
    protected abstract fun shouldFetch(data: ResultType?): Boolean

    @MainThread
    protected abstract fun loadFromDb(): LiveData<ResultType>?

    @MainThread
    protected abstract fun createCall(): LiveData<ApiResponse<RequestType>>

}