package com.trujis.common.utils

import android.content.Context
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager


class KeyboardUtils {

    companion object {

        fun isEnterClicked(keyCode: Int, event: KeyEvent): Boolean {
            return event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_ENTER
        }

        fun hideKeyboard(view: View?) {
            view?.let {
                val inputManager =
                    view.context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
                inputManager?.let {
                    inputManager.hideSoftInputFromWindow(view.windowToken, 0)
                }
            }
        }
    }
}