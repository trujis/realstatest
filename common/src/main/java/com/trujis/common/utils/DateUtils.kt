package com.trujis.common.utils

import android.os.Build
import androidx.annotation.RequiresApi
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

fun String.toDateFormatted(outputPattern: String? = null, inputPattern: String? = null): String? {
    val finalOutputPattern = outputPattern ?: DEFAULT_OUTPUT_PATTERN
    val finalInputPattern = inputPattern ?: DEFAULT_INPUT_PATTERN
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        parseAndroidO(this, finalOutputPattern, finalInputPattern)
    } else {
        parse(this, finalOutputPattern, finalInputPattern)
    }
}

@RequiresApi(Build.VERSION_CODES.O)
private fun parseAndroidO(date: String, outputPattern: String, inputPattern: String): String? {
    return try {
        val localDateTime = LocalDateTime.parse(date, DateTimeFormatter.ofPattern(inputPattern))
        val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern(outputPattern)
        formatter.format(localDateTime)
    } catch (e: Exception) {
        null
    }
}

private fun parse(date: String, outputPattern: String, inputPattern: String): String? {
    return try {
        val parser = SimpleDateFormat(inputPattern)
        val formatter = SimpleDateFormat(outputPattern)
        formatter.format(parser.parse(date))
    } catch (e: Exception) {
        null
    }
}

const val DEFAULT_OUTPUT_PATTERN = "dd/MM/yyyy"
const val DEFAULT_INPUT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss"