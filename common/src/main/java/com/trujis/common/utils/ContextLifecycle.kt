package com.trujis.common.utils

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner

fun LifecycleOwner.isResumed() = lifecycle.isResumed()

fun Lifecycle.isResumed() = currentState == Lifecycle.State.RESUMED
