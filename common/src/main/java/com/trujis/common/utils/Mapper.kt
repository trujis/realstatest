package com.trujis.common.utils

interface Mapper<FROM, TO> {

    fun map(input: FROM?): TO?

}
