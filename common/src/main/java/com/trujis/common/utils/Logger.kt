package com.trujis.common.utils

import timber.log.Timber
import java.lang.Exception

class Logger {

    companion object {

        fun initialize(isDebug: Boolean) {
            if (isDebug) {
                Timber.plant(Timber.DebugTree())
            }
        }

        fun d(message: String) = Timber.d(message)
        fun i(message: String) = Timber.i(message)
        fun w(message: String) = Timber.w(message)
        fun e(message: String, exception: Exception) = Timber.e(exception, message)

    }


}