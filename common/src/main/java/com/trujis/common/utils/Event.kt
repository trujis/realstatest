package com.trujis.common.utils

open class Event<out T>(private val content: T) {

    private var hasBeenHandled = false

    fun content(): T? {
        return if (hasBeenHandled) {
            null
        } else {
            hasBeenHandled = true
            content
        }
    }

    fun peekContent(): T = content

}