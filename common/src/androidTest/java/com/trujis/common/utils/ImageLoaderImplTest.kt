package com.trujis.common.utils

import android.widget.ImageView
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator
import com.trujis.common.R
import com.trujis.common.ui.utils.ImageLoaderImpl
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@RunWith(AndroidJUnit4::class)
class ImageLoaderImplTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var picasso: Picasso
    @Mock
    lateinit var requestCreator: RequestCreator
    @Mock
    lateinit var imageView: ImageView

    private lateinit var imageLoaderImpl: ImageLoaderImpl

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)

        Mockito.`when`(picasso.load(URL)).thenReturn(requestCreator)
        Mockito.`when`(requestCreator.placeholder(PLACEHOLDER)).thenReturn(requestCreator)
        Mockito.`when`(requestCreator.error(ERROR)).thenReturn(requestCreator)
        Mockito.`when`(requestCreator.fit()).thenReturn(requestCreator)

        imageLoaderImpl = ImageLoaderImpl()
    }

    @Test
    fun shouldNotLoadAnythingIfPicassoIsNotInjected() {
        //Act
        imageLoaderImpl.loadInto(imageView, URL)

        //Assert
        Mockito.verify(picasso, Mockito.times(0))
            .load(ArgumentMatchers.anyString())
    }

    @Test
    fun shouldLoadUsingDefaultPlaceholdersAndErrors() {
        //Arrange
        imageLoaderImpl.injectPicasso(picasso)
        Mockito.`when`(requestCreator.placeholder(R.drawable.ic_abstract_user_flat)).thenReturn(requestCreator)
        Mockito.`when`(requestCreator.error(R.drawable.ic_abstract_user_flat)).thenReturn(requestCreator)

        //Act
        imageLoaderImpl.loadInto(imageView, URL)

        //Assert
        Mockito.verify(picasso, Mockito.times(1)).load(URL)
        Mockito.verify(requestCreator, Mockito.times(1)).placeholder(R.drawable.ic_abstract_user_flat)
        Mockito.verify(requestCreator, Mockito.times(1)).error(R.drawable.ic_abstract_user_flat)
        Mockito.verify(requestCreator, Mockito.times(1)).fit()
        Mockito.verify(requestCreator, Mockito.times(1)).into(imageView)
    }

    @Test
    fun shouldLoadWithCustomPlaceholdersAndErrors() {
        //Arrange
        imageLoaderImpl.injectPicasso(picasso)

        //Act
        imageLoaderImpl.loadInto(imageView, URL, PLACEHOLDER, ERROR)

        //Assert
        Mockito.verify(picasso, Mockito.times(1)).load(URL)
        Mockito.verify(requestCreator, Mockito.times(1)).placeholder(PLACEHOLDER)
        Mockito.verify(requestCreator, Mockito.times(1)).error(ERROR)
        Mockito.verify(requestCreator, Mockito.times(1)).fit()
        Mockito.verify(requestCreator, Mockito.times(1)).into(imageView)
    }

    @Test
    fun shouldLoadWithoutPlaceholdersAndErrorsWhenRequested() {
        //Arrange
        imageLoaderImpl.injectPicasso(picasso)

        //Act
        imageLoaderImpl.loadInto(imageView, URL, 0, 0)

        //Assert
        Mockito.verify(picasso, Mockito.times(1)).load(URL)
        Mockito.verify(requestCreator, Mockito.times(0)).placeholder(ArgumentMatchers.anyInt())
        Mockito.verify(requestCreator, Mockito.times(0)).error(ArgumentMatchers.anyInt())
        Mockito.verify(requestCreator, Mockito.times(1)).fit()
        Mockito.verify(requestCreator, Mockito.times(1)).into(imageView)
    }

    companion object {
        const val URL = "URL"
        const val PLACEHOLDER = 1
        const val ERROR = 2
    }
}