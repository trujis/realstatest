# README #

Workflow:
https://trello.com/b/IMsAbK0d/real-statest

### Consider that... ###

* On a normal situation, I would delete all merged branches (except releases branches)

### Plugins used ###

* Dagger2
* Retrofit2
* Picasso
* Navigation with safeArgs
* Timber
* JUnit & Mockito
* LifeCycle
* Androidx

### Thanks to... ###

* https://github.com/github/gitignore/blob/master/Android.gitignore
* https://proandroiddev.com/multiple-ways-of-defining-clean-architecture-layers-bbb70afa5d4a
* https://federicotorres.medium.com/best-practice-for-implementing-dagger-in-multi-module-projects-1e6b3880140d
* https://medium.com/@freedom.chuks7/handling-events-the-mvvm-way-4c6ab6b93f20
* https://github.com/juanchosaravia/KedditBySteps/blob/master/app/src/main/java/com/droidcba/kedditbysteps/commons/InfiniteScrollListener.kt