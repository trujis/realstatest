package com.trujis.users.ui.mapper

import com.trujis.users.data.db.model.User
import com.trujis.users.ui.detail.UserDetail
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class UserDetailMapperImplTest {

    lateinit var mapper: UserDetailMapperImpl

    @Before
    fun before() {
        mapper = UserDetailMapperImpl()
    }

    @Test
    fun shouldMapNothingWhenMappingNullInput() {
        //Act
        val response = mapper.map(null)

        //Assert
        Assert.assertNull(response)
    }

    @Test
    fun shouldMapNameAsNameAndSurnameWhenMappingOneItemGivenInvalidRegisteredDate() {
        //Arrange
        val validRegisteredDate = false
        val input = getUser(validRegisteredDate)

        //Act
        val response = mapper.map(input)

        //Assert
        assertUser(response, validRegisteredDate)
    }

    @Test
    fun shouldMapNameAsNameAndSurnameWhenMappingOneItemGivenValidRegisteredDate() {
        //Arrange
        val validRegisteredDate = true
        val input = getUser(validRegisteredDate)

        //Act
        val response = mapper.map(input)

        //Assert
        assertUser(response, validRegisteredDate)
    }

    private fun getUser(withValidDate: Boolean) = User(
        1,
        "email",
        "name",
        "surname",
        "picture_l",
        "picture_m",
        "picture_t",
        "phone",
        "gender",
        "street",
        "city",
        "state",
        if (withValidDate) "2013-04-20T01:55:27.640Z" else "registeredDate",
        false
    )

    private fun assertUser(userDetail: UserDetail?, withValidDate: Boolean) {
        Assert.assertNotNull(userDetail)
        Assert.assertEquals(1, userDetail!!.id)
        Assert.assertEquals("gender", userDetail.gender)
        Assert.assertEquals("name", userDetail.name)
        Assert.assertEquals("surname", userDetail.surname)
        Assert.assertEquals("street", userDetail.street)
        Assert.assertEquals("city", userDetail.city)
        Assert.assertEquals("state", userDetail.state)
        Assert.assertEquals("email", userDetail.email)
        Assert.assertEquals(if (withValidDate) "20/04/2013" else "", userDetail.registeredDate)
        Assert.assertEquals("picture_l", userDetail.picture)
    }
}