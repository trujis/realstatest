package com.trujis.users.data.mapper

import com.trujis.users.data.api.model.*
import com.trujis.users.data.db.model.User
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class UserDbMapperImplTest {

    lateinit var mapper: UserDbMapperImpl

    @Before
    fun before() {
        mapper = UserDbMapperImpl()
    }

    @Test
    fun shouldMapNothingWhenMappingNullInput() {
        //Act
        val response = mapper.map(null)

        //Assert
        Assert.assertNull(response)
    }

    @Test
    fun shouldMapNothingWhenUserHasNoItems() {
        //Arrange
        val input = getValidUserItemResponse(0)

        //Act
        val response = mapper.map(input)

        //Assert
        Assert.assertNotNull(response)
        Assert.assertEquals(0, response!!.size)
    }

    @Test
    fun shouldMapNameAsNameAndSurnameWhenMappingOneItem() {
        //Arrange
        val repetitions = 3
        val input = getValidUserItemResponse(repetitions)

        //Act
        val response = mapper.map(input)

        //Assert
        Assert.assertNotNull(response)
        Assert.assertEquals(repetitions, response!!.size)
        repeat(repetitions) {
            check(response[it], it)
        }
    }

    private fun getValidUserItemResponse(repetitions: Int): UserResponse {
        val items = arrayListOf<UserItemResponse>()
        repeat(repetitions) { idx ->
            items.add(
                UserItemResponse(
                    "gender_$idx",
                    NameResponse("title_$idx", "name_$idx", "surname_$idx"),
                    LocationResponse(
                        StreetResponse("number_$idx", "nameStreet_$idx"),
                        "city_$idx",
                        "state_$idx"
                    ),
                    "email_$idx",
                    RegisterDateResponse("date_$idx", idx),
                    "phone_$idx",
                    ImagesResponse(
                        "large_$idx",
                        "medium_$idx",
                        "thumbnail_$idx"
                    )
                )
            )
        }
        return UserResponse(
            items,
            UserInfoResponse("seed", 900, 901, "version")
        )
    }

    private fun check(response: User, idx: Int) {
        Assert.assertNotNull(response)
        Assert.assertEquals(0, response.id)
        Assert.assertEquals("email_$idx", response.email)
        Assert.assertEquals("name_$idx", response.name)
        Assert.assertEquals("surname_$idx", response.surname)
        Assert.assertEquals("large_$idx", response.pictureLarge)
        Assert.assertEquals("medium_$idx", response.pictureMedium)
        Assert.assertEquals("thumbnail_$idx", response.pictureThumbnail)
        Assert.assertEquals("phone_$idx", response.phone)
        Assert.assertEquals("gender_$idx", response.gender)
        Assert.assertEquals("nameStreet_$idx", response.street)
        Assert.assertEquals("city_$idx", response.city)
        Assert.assertEquals("state_$idx", response.state)
        Assert.assertEquals("date_$idx", response.registeredDate)
        Assert.assertEquals(false, response.isRemoved)
    }

}