package com.trujis.users.ui.mapper

import com.trujis.users.data.db.model.User
import com.trujis.users.ui.home.UserList
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class UserListMapperImplTest {

    lateinit var mapper: UserListMapperImpl

    @Before
    fun before() {
        mapper = UserListMapperImpl()
    }

    @Test
    fun shouldMapNothingWhenMappingNullInput() {
        //Act
        val response = mapper.map(null)

        //Assert
        Assert.assertNull(response)
    }

    @Test
    fun shouldMapNothingWhenUserHasNoItems() {
        //Arrange
        val input = getValidUserList(0)

        //Act
        val response = mapper.map(input)

        //Assert
        Assert.assertNotNull(response)
        Assert.assertEquals(0, response!!.size)
    }

    @Test
    fun shouldMapNameAsNameAndSurnameWhenMappingOneItem() {
        //Arrange
        val repetitions = 3
        val input = getValidUserList(repetitions)

        //Act
        val response = mapper.map(input)

        //Assert
        Assert.assertNotNull(response)
        Assert.assertEquals(repetitions, response!!.size)
        repeat(repetitions) {
            check(response[it], it)
        }
    }

    private fun getValidUserList(repetitions: Int): List<User> {
        val items = arrayListOf<User>()
        repeat(repetitions) { idx ->
            items.add(
                User(
                    idx,
                    "email_$idx",
                    "name_$idx",
                    "surname_$idx",
                    "picture_l_$idx",
                    "picture_m_$idx",
                    "picture_t_$idx",
                    "phone_$idx",
                    "gender_$idx",
                    "street_$idx",
                    "city_$idx",
                    "state_$idx",
                    "registeredDate_$idx",
                    false
                )
            )
        }
        return items
    }

    private fun check(response: UserList, idx: Int) {
        Assert.assertNotNull(response)
        Assert.assertEquals(idx, response.id)
        Assert.assertEquals("name_$idx", response.name)
        Assert.assertEquals("surname_$idx", response.surname)
        Assert.assertEquals("email_$idx", response.email)
        Assert.assertEquals("phone_$idx", response.phone)
        Assert.assertEquals("picture_l_$idx", response.picture)
    }

}