package com.trujis.users.data.db

import android.content.Context
import android.os.Handler
import android.os.Looper
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.trujis.users.data.db.model.User
import com.trujis.users.util.observeOnce
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException
import kotlin.jvm.Throws


@RunWith(AndroidJUnit4::class)
class UsersDaoTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var userDao: UsersDao
    private lateinit var db: RealStaTestDB

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, RealStaTestDB::class.java
        ).build()
        userDao = db.userDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    fun findUserWhenOneUserInsertedOnDBWithoutAnyFilters() {
        //Arrange
        val repeat = 5
        userDao.insert(getUser(1))

        //Act
        val dbStore = userDao.findAll("")

        //Assert
        dbStore.observeOnce {
            assertEquals(1, it.size)
            assertNotNull(it[0])
            checkUser(it[0], 1)
        }
    }

    @Test
    fun findAllUsersWhenManyUsersInsertedOnDBWithoutAnyFilters() {
        //Arrange
        val repeat = 5
        insertUsers(repeat)

        //Act
        val dbStore = userDao.findAll("")

        //Assert
        dbStore.observeOnce {
            assertEquals(repeat, it.size)
            it.forEachIndexed { idx, user ->
                assertNotNull(it)
                checkUser(user, idx)
            }
        }
    }

    @Test
    fun findAllNonRemovedUsersWhenManyUsersInsertedOnDBWithoutAnyFilters() {
        //Arrange
        val handler = Handler(Looper.getMainLooper())
        val repeat = 5
        insertUsers(repeat)

        //Act & Assert
        userDao.remove(0)
        val runnable = Runnable {
            val dbStore = userDao.findAll("")
            dbStore.observeOnce {
                assertEquals(repeat-1, it.size)
                it.forEachIndexed { idx, user ->
                    assertNotNull(it)
                    checkUser(user, idx+1)
                }
            }
        }
        handler.postDelayed(runnable, 1000L)
    }

    @Test
    fun findAllUsersThatMatchWithFilters() {
        //Arrange
        insertUsers(10)

        //Act
        val dbStore = userDao.findAll("mail_0")

        //Assert
        dbStore.observeOnce {
            assertEquals(1, it.size)
            assertNotNull(it[0])
            checkUser(it[0], 0)
        }
    }

    @Test
    fun findSpecificUserThatMatchWithId() {
        //Arrange
        insertUsers(10)

        //Act
        val dbStore = userDao.find(5)

        //Assert
        dbStore.observeOnce {
            assertNotNull(it)
            checkUser(it, 4)
        }
    }

    private fun insertUsers(userNumber: Int) {
        val users = arrayListOf<User>()
        repeat(userNumber) { idx ->
            users.add(getUser(idx))
        }
        userDao.insertItems(users)
    }

    private fun getUser(id: Int): User {
        return User(
            0,
            "email_$id",
            "name_$id",
            "surname_$id",
            "p_large_$id",
            "p_medium_$id",
            "p_thumb_$id",
            "phone_$id",
            "gender_$id",
            "street_$id",
            "city_$id",
            "state_$id",
            "registered_$id", false)
    }

    private fun checkUser(it: User?, idx: Int) {
        assertNotNull(it)
        assertEquals("email_$idx", it!!.email)
        assertEquals("name_$idx", it.name)
        assertEquals("surname_$idx", it.surname)
        assertEquals("p_large_$idx", it.pictureLarge)
        assertEquals("p_medium_$idx", it.pictureMedium)
        assertEquals("p_thumb_$idx", it.pictureThumbnail)
        assertEquals("phone_$idx", it.phone)
        assertEquals("gender_$idx", it.gender)
        assertEquals("street_$idx", it.street)
        assertEquals("city_$idx", it.city)
        assertEquals("state_$idx", it.state)
        assertEquals("registered_$idx", it.registeredDate)
        assertEquals(false, it.isRemoved)
    }
}