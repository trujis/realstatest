package com.trujis.users.ui.mapper

import com.trujis.common.utils.Mapper
import com.trujis.users.data.db.model.User
import com.trujis.users.ui.home.UserList
import javax.inject.Inject

class UserListMapperImpl @Inject constructor() : Mapper<List<User>, List<UserList>> {

    override fun map(input: List<User>?): List<UserList>? {
        return input?.map { item ->
            UserList(
                item.id,
                item.name ?: "",
                item.surname ?: "",
                item.email,
                item.phone ?: "",
                item.pictureLarge ?: ""
            )
        }
    }

}