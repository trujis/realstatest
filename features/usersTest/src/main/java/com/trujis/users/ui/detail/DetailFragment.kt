package com.trujis.users.ui.detail

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.trujis.common.ui.utils.ImageLoaderImpl
import com.trujis.common.ui.utils.ImagePolicy
import com.trujis.users.R
import com.trujis.users.databinding.FragmentDetailBinding
import com.trujis.users.di.provider.UsersTestInjectorProvider
import com.trujis.users.ui.ToolbarBaseFragment
import javax.inject.Inject

class DetailFragment : ToolbarBaseFragment<Any, FragmentDetailBinding>(R.layout.fragment_detail) {

    private lateinit var detailViewModel: DetailViewModel
    private val args: DetailFragmentArgs by navArgs()

    @Inject
    lateinit var imageLoader: ImageLoaderImpl

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity?.applicationContext as UsersTestInjectorProvider).usersTestInjector().inject(this)
        initViewModelAndObservers()
        loadArguments()
        configToolbar(true, getString(R.string.fragment_title_detail))
    }

    private fun loadArguments() {
        detailViewModel.retrieveUser(args.userId)
    }

    private fun initViewModelAndObservers() {
        detailViewModel = provideViewModel(DetailViewModel::class.java)
        binding.viewModel = detailViewModel
        detailViewModel.user.observe(viewLifecycleOwner, Observer { user ->
            imageLoader
                .setPolicy(ImagePolicy.OFFLINE)
                .loadInto(binding.civUser, user.picture)
        })
    }

}