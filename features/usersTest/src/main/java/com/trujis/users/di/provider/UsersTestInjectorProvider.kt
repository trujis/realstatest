package com.trujis.users.di.provider

import com.trujis.users.di.UsersTestComponent

interface UsersTestInjectorProvider {
    fun usersTestInjector(): UsersTestComponent
}