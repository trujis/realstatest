package com.trujis.users.ui.home

import androidx.lifecycle.*
import com.trujis.common.data.Resource
import com.trujis.common.data.Status
import com.trujis.common.utils.Event
import com.trujis.common.utils.Mapper
import com.trujis.users.data.api.model.UserRequest
import com.trujis.users.data.db.model.User
import com.trujis.users.domain.repository.UserRepository
import javax.inject.Inject

class UserListViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val mapper: Mapper<List<User>, List<UserList>>
) : ViewModel() {

    val showLoading = MutableLiveData<Event<Boolean>>()
    val showError = MutableLiveData<Event<String?>>()
    val userList = MediatorLiveData<List<UserList>>()
    val scrollToTop = MediatorLiveData<Event<Unit>>()

    private var shouldScrollToTop = false
    private val getMoreUsers: MutableLiveData<Event<String?>> = MutableLiveData()
    private var requestMoreUsers: LiveData<Resource<List<User>>> = Transformations
        .switchMap(getMoreUsers) {event ->
            val lastId = getLastLoadedId()
            userRepository.list(UserRequest(lastId, event.content()))
        }

    init {
        userList.addSource(requestMoreUsers) { response ->
            showLoading.value = Event(response.status == Status.LOADING)
            when (response.status) {
                Status.SUCCESS -> {
                    postUserItems(response.data)
                }
                Status.ERROR -> {
                    showError.value = Event<String?>(null)
                }
                else -> {
                }
            }
        }
    }

    fun postUserItems(users: List<User>?) {
        userList.value = mapper.map(users)
        if (shouldScrollToTop) scrollToTop.value = Event(Unit)
        shouldScrollToTop = false
    }

    fun retrieveUsers(filter: String? = null) {
        shouldScrollToTop = filter != null
        getMoreUsers.value = Event(filter)
    }

    fun onRemoveUser(item: UserList) {
        userRepository.removeUser(item.id)
    }

    private fun getLastLoadedId(): Int? {
        return userList.value?.let { list ->
            if (list.isNotEmpty()) list.last().id else null
        }
    }
}