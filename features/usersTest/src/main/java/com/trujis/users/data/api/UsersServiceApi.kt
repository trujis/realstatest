package com.trujis.users.data.api

import androidx.lifecycle.LiveData
import com.trujis.common.data.api.ApiResponse
import com.trujis.users.data.api.model.UserResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface UsersServiceApi {

    @GET("api/")
    abstract fun get(
        @Query("inc") include: String,
        @Query("results") results: Int
    ): LiveData<ApiResponse<UserResponse>>

}