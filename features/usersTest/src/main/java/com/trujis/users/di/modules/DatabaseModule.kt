package com.trujis.users.di.modules

import android.app.Application
import androidx.room.Room
import com.trujis.common.di.scope.FeatureScope
import com.trujis.users.data.db.RealStaTestDB
import com.trujis.users.data.db.UsersDao
import dagger.Module
import dagger.Provides

@Module
class DatabaseModule {


    @FeatureScope
    @Provides
    fun provideDb(app: Application): RealStaTestDB {
        return Room.databaseBuilder(app, RealStaTestDB::class.java, "realstatest.db")
            .build()
    }

    @FeatureScope
    @Provides
    fun provideUsersDao(db: RealStaTestDB): UsersDao {
        return db.userDao()
    }

}