package com.trujis.users.di.modules

import com.trujis.common.utils.Mapper
import com.trujis.users.data.api.model.UserResponse
import com.trujis.users.data.db.model.User
import com.trujis.users.data.mapper.UserDbMapperImpl
import com.trujis.users.ui.detail.UserDetail
import com.trujis.users.ui.home.UserList
import com.trujis.users.ui.mapper.UserDetailMapperImpl
import com.trujis.users.ui.mapper.UserListMapperImpl
import dagger.Module
import dagger.Provides


@Module
class UsersTestModule {

    @Provides
    fun provideUserListMapper(): Mapper<List<User>, List<UserList>> {
        return UserListMapperImpl()
    }

    @Provides
    fun provideUserDbMapper(): Mapper<UserResponse, List<User>> {
        return UserDbMapperImpl()
    }

    @Provides
    fun provideUserDetailMapper(): Mapper<User, UserDetail> {
        return UserDetailMapperImpl()
    }

}