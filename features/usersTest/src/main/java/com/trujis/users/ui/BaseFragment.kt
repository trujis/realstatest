package com.trujis.users.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.trujis.common.utils.autoCleared
import com.trujis.users.di.provider.UsersTestInjectorProvider

abstract class BaseFragment<AdapterType : Any, FragmentBinding : ViewDataBinding> constructor(
    private val fragmentId: Int
) : Fragment() {

    var binding by autoCleared<FragmentBinding>()
    var adapter by autoCleared<AdapterType>()

    protected fun <T : ViewModel> provideViewModel(viewModel: Class<T>): T {
        return ViewModelProviders.of(
            this,
            (activity?.applicationContext as UsersTestInjectorProvider).usersTestInjector().viewModelFactory
        ).get(viewModel)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val dataBinding =
            DataBindingUtil.inflate<FragmentBinding>(inflater, fragmentId, container, false)
        binding = dataBinding
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.lifecycleOwner = viewLifecycleOwner
    }

    protected fun loading(showLoading: Boolean) {
        (activity as MainActivity).loading(showLoading)
    }

    protected fun showError(message: String?) {
        (activity as MainActivity).showError(message)
    }
}