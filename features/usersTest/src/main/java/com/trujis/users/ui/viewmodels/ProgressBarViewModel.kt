package com.trujis.users.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class ProgressBarViewModel @Inject constructor(): ViewModel() {
    val progressVisibility = MutableLiveData<Boolean>()

    fun setProgressBarVisibility(visible: Boolean){
        progressVisibility.value = visible
    }
}