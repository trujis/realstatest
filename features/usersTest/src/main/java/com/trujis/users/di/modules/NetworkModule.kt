package com.trujis.users.di.modules

import com.trujis.common.data.api.adapter.LiveDataCallAdapterFactory
import com.trujis.users.BuildConfig
import com.trujis.users.data.api.UsersServiceApi
import com.trujis.users.domain.repository.UserRepository
import com.trujis.users.data.repository.UserRepositoryImpl
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetworkModule {

    @Provides
    fun getRetrofit(okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder().baseUrl(BuildConfig.BASE_URL).client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .build()

    @Provides
    fun getUserApi(retrofit: Retrofit): UsersServiceApi =
        retrofit.create(UsersServiceApi::class.java)

    @Provides
    fun provideRepository(userRepositoryImpl: UserRepositoryImpl): UserRepository = userRepositoryImpl
}