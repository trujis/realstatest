package com.trujis.users.data.api.model

import com.google.gson.annotations.SerializedName

data class NameResponse(
    @SerializedName("title")
    val title: String,
    @SerializedName("first")
    val name: String,
    @SerializedName("last")
    val surname: String
) {

    override fun toString(): String {
        return "NameResponse(title='$title', name='$name', surname='$surname')"
    }

}