package com.trujis.users.ui.detail

data class UserDetail(
    val id: Int,
    val gender: String,
    val name: String,
    val surname: String,
    val street: String,
    val city: String,
    val state: String,
    val email: String,
    val registeredDate: String,
    val picture: String
) {

    override fun toString(): String {
        return "UserDetail(id='$id', gender='$gender', name='$name', surname='$surname', street='$street', city='$city', state='$state', email='$email', registeredDate='$registeredDate', picture='$picture')"
    }

}