package com.trujis.users.data.api.model

import com.google.gson.annotations.SerializedName

data class ImagesResponse(
    @SerializedName("large")
    val large: String,
    @SerializedName("medium")
    val medium: String,
    @SerializedName("thumbnail")
    val thumbnail: String
) {

    override fun toString(): String {
        return "ImagesResponse(large='$large', medium='$medium', thumbnail='$thumbnail')"
    }

}