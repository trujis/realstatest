package com.trujis.users.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.trujis.users.data.db.model.User

@Dao
interface UsersDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(item: User)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertItems(data: List<User>)

    @Query("UPDATE users set is_removed = 1 WHERE id = :id")
    fun remove(id: Int)

    @Query("SELECT * FROM users WHERE is_removed = 0 AND ('' = :filter OR name LIKE '%' || :filter || '%' OR surname LIKE '%' || :filter || '%' OR email LIKE '%' || :filter || '%')")
    fun findAll(filter: String): LiveData<List<User>>

    @Query("SELECT * FROM users WHERE id = :id AND is_removed = 0")
    fun find(id: Int): LiveData<User>

}