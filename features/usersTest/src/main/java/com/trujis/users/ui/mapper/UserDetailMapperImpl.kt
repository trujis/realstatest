package com.trujis.users.ui.mapper

import android.os.Build
import androidx.annotation.RequiresApi
import com.trujis.common.utils.Mapper
import com.trujis.common.utils.toDateFormatted
import com.trujis.users.data.db.model.User
import com.trujis.users.ui.detail.UserDetail
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import javax.inject.Inject

class UserDetailMapperImpl @Inject constructor() : Mapper<User, UserDetail> {

    override fun map(input: User?): UserDetail? {
        return input?.let { item ->
            UserDetail(
                item.id,
                item.gender ?: "",
                item.name ?: "",
                item.surname ?: "",
                item.street ?: "",
                item.city ?: "",
                item.state ?: "",
                item.email,
                item.registeredDate?.toDateFormatted() ?: "",
                item.pictureLarge ?: ""
            )
        }
    }

}