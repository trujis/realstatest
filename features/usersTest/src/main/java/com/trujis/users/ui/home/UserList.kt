package com.trujis.users.ui.home

data class UserList(
    val id: Int,
    val name: String,
    val surname: String,
    val email: String,
    val phone: String,
    val picture: String
) {

    override fun toString(): String {
        return "UserList(id= '$id', name='$name', surname='$surname', email='$email', phone='$phone', picture='$picture')"
    }

}