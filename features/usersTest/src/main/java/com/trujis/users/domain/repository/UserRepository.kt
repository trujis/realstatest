package com.trujis.users.domain.repository

import androidx.lifecycle.LiveData
import com.trujis.common.data.Resource
import com.trujis.users.data.api.model.UserRequest
import com.trujis.users.data.db.model.User

interface UserRepository {

    fun list(request: UserRequest): LiveData<Resource<List<User>>>

    fun get(id: Int): LiveData<User>

    fun removeUser(id: Int)

}
