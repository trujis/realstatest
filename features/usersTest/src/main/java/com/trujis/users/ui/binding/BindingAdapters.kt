package com.trujis.users.ui.binding

import androidx.databinding.BindingAdapter
import com.trujis.users.ui.components.ProgressBarComponent

/**
 * Data Binding adapters specific to the app.
 */
object BindingAdapters {

    @JvmStatic
    @BindingAdapter("show_progress")
    fun setProgressBarVisibility(component: ProgressBarComponent, visible: Boolean?) {
        component.setProgressBarVisibility(visible)
    }

}