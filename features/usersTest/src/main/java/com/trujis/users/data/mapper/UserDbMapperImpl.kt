package com.trujis.users.data.mapper

import com.trujis.common.utils.Mapper
import com.trujis.users.data.api.model.UserResponse
import com.trujis.users.data.db.model.User
import javax.inject.Inject

class UserDbMapperImpl @Inject constructor() : Mapper<UserResponse, List<User>> {

    override fun map(input: UserResponse?): List<User>? {
        return input?.results?.map { item ->
            User(
                0,
                item.email,
                item.name.name,
                item.name.surname,
                item.picture.large,
                item.picture.medium,
                item.picture.thumbnail,
                item.phone,
                item.gender,
                item.location.street.name,
                item.location.city,
                item.location.state,
                item.registered.date,
                isRemoved = false
            )
        }
    }
}

