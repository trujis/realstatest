package com.trujis.users.data.api.model

import com.google.gson.annotations.SerializedName

data class LocationResponse(
    @SerializedName("street")
    val street: StreetResponse,
    @SerializedName("city")
    val city: String,
    @SerializedName("state")
    val state: String
) {

    override fun toString(): String {
        return "LocationResponse(street=$street, city='$city', state='$state')"
    }

}