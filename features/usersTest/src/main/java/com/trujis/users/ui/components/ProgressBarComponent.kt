package com.trujis.users.ui.components

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.trujis.users.R

class ProgressBarComponent(context: Context, attributeSet: AttributeSet?) :
    FrameLayout(context, attributeSet) {

    init {
        LayoutInflater.from(context).inflate(R.layout.component_progress_bar, this, true)
    }

    fun setProgressBarVisibility(visible: Boolean?) {
        visible?.let {
            findViewById<FrameLayout>(R.id.progress_container).apply {
                visibility = if (visible) {
                    View.VISIBLE
                } else {
                    View.GONE
                }
            }
        }
    }
}