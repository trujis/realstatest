package com.trujis.users.ui.detail

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import com.trujis.common.utils.Mapper
import com.trujis.users.data.db.model.User
import com.trujis.users.domain.repository.UserRepository
import javax.inject.Inject

class DetailViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val mapper: Mapper<User, UserDetail>
): ViewModel() {

    val user = MediatorLiveData<UserDetail>()
    val isMale = MediatorLiveData<Boolean>()

    fun retrieveUser(id: Int) {
        user.addSource(userRepository.get(id)) { response ->
            user.value = mapper.map(response)
            isMale.value = response.gender == "male"
        }
    }
}