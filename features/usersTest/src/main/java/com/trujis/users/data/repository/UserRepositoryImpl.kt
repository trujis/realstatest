package com.trujis.users.data.repository

import androidx.lifecycle.LiveData
import com.trujis.common.data.Resource
import com.trujis.common.data.api.ApiResponse
import com.trujis.common.data.repository.NetworkBoundResource
import com.trujis.common.utils.AppExecutors
import com.trujis.common.utils.Mapper
import com.trujis.users.data.api.UsersServiceApi
import com.trujis.users.data.api.model.UserRequest
import com.trujis.users.data.api.model.UserResponse
import com.trujis.users.data.db.UsersDao
import com.trujis.users.data.db.model.User
import com.trujis.users.domain.repository.UserRepository
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(
    private val usersServiceApi: UsersServiceApi,
    private val appExecutors: AppExecutors,
    private val usersDao: UsersDao,
    private val mapper: Mapper<UserResponse, List<User>>
) : UserRepository {

    override fun list(request: UserRequest): LiveData<Resource<List<User>>> {
        return object : NetworkBoundResource<List<User>, UserResponse>(appExecutors) {
            override fun saveCallResult(item: UserResponse) {
                mapper.map(item)?.let { users ->
                    usersDao.insertItems(users)
                }
            }

            override fun shouldFetch(data: List<User>?): Boolean {
                if (request.filter != null) return false

                return if (data?.isEmpty() == false) {
                    data.last().id <= request.minId ?: 0
                } else {
                    true
                }
            }

            override fun loadFromDb(): LiveData<List<User>>? {
                return usersDao.findAll(request.filter ?: "")
            }

            override fun createCall(): LiveData<ApiResponse<UserResponse>> =
                usersServiceApi.get(request.include, request.results)

        }.asLiveData()
    }

    override fun removeUser(id: Int) {
        appExecutors.diskIO().execute {
            usersDao.remove(id)
        }
    }

    override fun get(id: Int): LiveData<User> {
        return usersDao.find(id)
    }

}