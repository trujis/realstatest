package com.trujis.users.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.trujis.users.data.db.model.User

@Database(
    entities = [User::class],
    version = 1,
    exportSchema = false
)
abstract class RealStaTestDB : RoomDatabase() {

    abstract fun userDao(): UsersDao

}