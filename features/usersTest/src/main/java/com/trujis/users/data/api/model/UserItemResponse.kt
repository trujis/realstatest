package com.trujis.users.data.api.model

import com.google.gson.annotations.SerializedName

data class UserItemResponse(
    @SerializedName("gender")
    val gender: String,
    @SerializedName("name")
    val name: NameResponse,
    @SerializedName("location")
    val location: LocationResponse,
    @SerializedName("email")
    val email: String,
    @SerializedName("registered")
    val registered: RegisterDateResponse,
    @SerializedName("phone")
    val phone: String,
    @SerializedName("picture")
    val picture: ImagesResponse
) {

    override fun toString(): String {
        return "UserResponse(gender='$gender', name=$name, location=$location, email='$email', registered=$registered, phone='$phone', picture=$picture)"
    }
    
}