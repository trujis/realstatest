package com.trujis.users.di

import android.app.Application
import androidx.lifecycle.ViewModelProvider
import com.trujis.common.di.CommonComponent
import com.trujis.common.di.scope.FeatureScope
import com.trujis.users.di.modules.DatabaseModule
import com.trujis.users.di.modules.NetworkModule
import com.trujis.users.di.modules.UsersTestModule
import com.trujis.users.di.modules.ViewModelModule
import com.trujis.users.ui.detail.DetailFragment
import com.trujis.users.ui.home.HomeFragment
import dagger.BindsInstance
import dagger.Component


@Component(
    modules = [ViewModelModule::class, NetworkModule::class, UsersTestModule::class, DatabaseModule::class],
    dependencies = [CommonComponent::class]
)
@FeatureScope
interface UsersTestComponent {

    val viewModelFactory: ViewModelProvider.Factory

    @Component.Factory
    interface Factory {
        fun create(
            coreComponent: CommonComponent,
            @BindsInstance app: Application
        ): UsersTestComponent
    }

    fun inject(fragment: HomeFragment)
    fun inject(fragment: DetailFragment)
    fun inject(application: Application)

}