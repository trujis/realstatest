package com.trujis.users.data.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "users",
    indices = [Index(value = ["email"], unique = true)]
)
data class User(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "email") val email: String,
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "surname") val surname: String?,
    @ColumnInfo(name = "picture_large") val pictureLarge: String?,
    @ColumnInfo(name = "picture_medium") val pictureMedium: String?,
    @ColumnInfo(name = "picture_thumbnail") val pictureThumbnail: String?,
    @ColumnInfo(name = "phone") val phone: String?,
    @ColumnInfo(name = "gender") val gender: String?,
    @ColumnInfo(name = "street") val street: String?,
    @ColumnInfo(name = "city") val city: String?,
    @ColumnInfo(name = "state") val state: String?,
    @ColumnInfo(name = "registered_date") val registeredDate: String?,
    @ColumnInfo(name = "is_removed") val isRemoved: Boolean?
)