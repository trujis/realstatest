package com.trujis.users.data.api.model

import com.google.gson.annotations.SerializedName

data class RegisterDateResponse(
    @SerializedName("date")
    val date: String,
    @SerializedName("age")
    val age: Int
) {

    override fun toString(): String {
        return "RegisterDateResponse(date='$date', age=$age)"
    }
    
}