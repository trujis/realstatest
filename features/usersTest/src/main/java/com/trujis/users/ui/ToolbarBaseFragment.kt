package com.trujis.users.ui

import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.ViewDataBinding

open class ToolbarBaseFragment<AdapterType : Any, FragmentBinding : ViewDataBinding>(fragmentId: Int) :
    BaseFragment<AdapterType, FragmentBinding>(fragmentId) {

    private fun setToolbarVisibility(show: Boolean) {
        (activity as? AppCompatActivity)?.let {
            if (show) it.supportActionBar?.show() else it.supportActionBar?.hide()
        }
    }

    private fun showNavigationButton(shouldShowNavigationButton: Boolean) {
        (activity as? AppCompatActivity)?.supportActionBar?.setDisplayShowHomeEnabled(
            shouldShowNavigationButton
        )
        (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(
            shouldShowNavigationButton
        )
    }

    fun configToolbar(shouldShowNavigationButton: Boolean, title: String) {
        setToolbarVisibility(true)
        showNavigationButton(shouldShowNavigationButton)
        (activity as? AppCompatActivity)?.supportActionBar?.title = title
    }

    fun hideToolbar() {
        setToolbarVisibility(false)
    }

}