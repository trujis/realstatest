package com.trujis.users.data.api.model

import com.google.gson.annotations.SerializedName

data class StreetResponse(
    @SerializedName("number")
    val number: String,
    @SerializedName("name")
    val name: String
) {

    override fun toString(): String {
        return "StreetResponse(number='$number', name='$name')"
    }

}