package com.trujis.users.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.trujis.common.ui.adapters.DataBoundListAdapter
import com.trujis.common.ui.utils.ImageLoader
import com.trujis.common.utils.AppExecutors
import com.trujis.users.databinding.UserItemBinding

class UserAdapter(
    appExecutors: AppExecutors,
    private val imageLoader: ImageLoader,
    private val onUserClick: (UserList) -> Unit,
    private val onUserTrashClick: (UserList) -> Unit,
) : DataBoundListAdapter<UserList, UserItemBinding>(
    appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<UserList>() {
        override fun areItemsTheSame(oldItem: UserList, newItem: UserList): Boolean {
            return oldItem.email == newItem.email
        }

        override fun areContentsTheSame(oldItem: UserList, newItem: UserList): Boolean {
            return oldItem == newItem
        }
    }
) {

    override fun createBinding(parent: ViewGroup): UserItemBinding {
        return UserItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    }

    override fun bind(binding: UserItemBinding, item: UserList) {
        binding.user = item
        bindImage(binding, item)
        bindOnClick(binding, item)
        bindOnClickTrash(binding, item)
    }

    private fun bindOnClickTrash(binding: UserItemBinding, item: UserList) {
        binding.ivTrash.setOnClickListener {
            onUserTrashClick.invoke(item)
        }
    }

    private fun bindOnClick(binding: UserItemBinding, item: UserList) {
        binding.cardView.setOnClickListener {
            onUserClick.invoke(item)
        }
    }

    private fun bindImage(binding: UserItemBinding, item: UserList) {
        if (item.picture.isNotEmpty()) {
            imageLoader.loadInto(binding.ivImage, item.picture)
        }
    }

}