package com.trujis.users.ui

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.google.android.material.snackbar.Snackbar
import com.trujis.common.utils.KeyboardUtils
import com.trujis.users.R
import com.trujis.users.databinding.ActivityMainBinding
import com.trujis.users.di.provider.UsersTestInjectorProvider
import com.trujis.users.ui.viewmodels.ProgressBarViewModel

class MainActivity : AppCompatActivity() {

    private fun injector() =
        (this.applicationContext as UsersTestInjectorProvider).usersTestInjector()

    lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var progressBarViewModel: ProgressBarViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModelAndObservers()
        setupBinding()
        setSupportActionBar(binding.toolbar)
        setupNavigationComponents()
    }

    private fun initViewModelAndObservers() {
        progressBarViewModel =
            ViewModelProviders.of(this, injector().viewModelFactory)
                .get(ProgressBarViewModel::class.java)
    }

    private fun setupBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.progressBarViewModel = progressBarViewModel
    }

    fun loading(showLoading: Boolean) {
        binding.progressBar.setProgressBarVisibility(showLoading)
    }

    fun showError(message: String? = null) {
        KeyboardUtils.hideKeyboard(binding.root)
        val snackBar = Snackbar.make(
            binding.root,
            message ?: getString(R.string.generic_error),
            Snackbar.LENGTH_LONG
        )
        snackBar.show()
    }

    private fun setupNavigationComponents() {
        setupNavController()
        linkNavigationComponents()
    }

    private fun setupNavController() {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
    }

    private fun linkNavigationComponents() {
        appBarConfiguration =
            AppBarConfiguration(navController.graph)
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return true
    }

}

object DestinationHelper {

    val TOP_LEVEL_DESTINATIONS = setOf(
        R.id.homeFragment,
    )

}