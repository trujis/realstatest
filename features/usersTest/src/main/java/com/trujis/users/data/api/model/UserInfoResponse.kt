package com.trujis.users.data.api.model

import com.google.gson.annotations.SerializedName

data class UserInfoResponse(
    @SerializedName("seed")
    val seed: String,
    @SerializedName("results")
    val results: Int,
    @SerializedName("page")
    val page: Int,
    @SerializedName("version")
    val version: String
) {

    override fun toString(): String {
        return "UserInfoResponse(seed='$seed', results=$results, page=$page, version='$version')"
    }
    
}