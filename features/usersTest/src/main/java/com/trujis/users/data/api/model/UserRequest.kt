package com.trujis.users.data.api.model

data class UserRequest(
    val minId: Int? = 0,
    val filter: String? = ""
) {

    val include = "name,email,picture,phone,gender,location,registered,email"
    val results: Int = 40

    override fun toString(): String {
        return "UserRequest(results=$results, minId=$minId, filter=$filter, include='$include')"
    }

}