package com.trujis.users.ui.home

import android.os.Bundle
import android.text.Editable
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.trujis.common.ui.common.InfiniteScrollListener
import com.trujis.common.ui.utils.ImageLoader
import com.trujis.common.ui.utils.RecyclerViewUtils.scrollDelayedToPosition
import com.trujis.common.ui.utils.TextInputEditTextUtils.addDelayedTextChangedListener
import com.trujis.common.utils.AppExecutors
import com.trujis.common.utils.isResumed
import com.trujis.users.R
import com.trujis.users.databinding.FragmentHomeBinding
import com.trujis.users.di.provider.UsersTestInjectorProvider
import com.trujis.users.ui.ToolbarBaseFragment
import javax.inject.Inject

class HomeFragment : ToolbarBaseFragment<Any, FragmentHomeBinding>(R.layout.fragment_home) {

    @Inject
    lateinit var appExecutors: AppExecutors

    @Inject
    lateinit var imageLoader: ImageLoader
    private lateinit var userListViewModel: UserListViewModel
    private lateinit var userAdapter: UserAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configToolbar(false, getString(R.string.fragment_title_home))
        (activity?.applicationContext as UsersTestInjectorProvider).usersTestInjector().inject(this)
        initViewModelAndObservers()
        setupViews()
    }

    private fun initViewModelAndObservers() {
        userListViewModel = provideViewModel(UserListViewModel::class.java)
        initObservers()
        userListViewModel.retrieveUsers()
    }

    private fun initObservers() {
        userListViewModel.showLoading.observe(viewLifecycleOwner, Observer { event ->
            event?.content()?.let { showLoading ->
                loading(showLoading)
            }
        })
        userListViewModel.showError.observe(viewLifecycleOwner, Observer { event ->
            showError(event?.content())
        })

        userListViewModel.userList.observe(viewLifecycleOwner, Observer { response ->
            if (viewLifecycleOwner.isResumed()) {
                userAdapter.submitList(response)
            }
        })

        userListViewModel.scrollToTop.observe(viewLifecycleOwner, Observer { event ->
            event.content()?.let { binding.rvUserList.scrollDelayedToPosition(0) }
        })
    }

    private fun setupViews() {
        binding.apply {
            userAdapter = UserAdapter(
                appExecutors,
                imageLoader,
                this@HomeFragment::onClickUser,
                this@HomeFragment::onClickUserTrash
            )
            rvUserList.apply {
                adapter = userAdapter
                val linearLayoutManager =
                    LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                layoutManager = linearLayoutManager
                itemAnimator = DefaultItemAnimator()
                addOnScrollListener(
                    InfiniteScrollListener(
                        { userListViewModel.retrieveUsers() },
                        linearLayoutManager
                    )
                )
            }

            tietFilter.addDelayedTextChangedListener(this@HomeFragment::onTextFilterChanged)
        }
    }

    private fun onClickUser(item: UserList) {
        val directions = HomeFragmentDirections.actionHomeFragmentToDetailFragment(item.id)
        findNavController(this).navigate(directions)
    }

    private fun onClickUserTrash(item: UserList) {
        userListViewModel.onRemoveUser(item)
    }

    private fun onTextFilterChanged(editable: Editable?) {
        userListViewModel.retrieveUsers(editable?.toString())
    }

}