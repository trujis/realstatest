package com.trujis.users.data.api.model

import com.google.gson.annotations.SerializedName

data class UserResponse(
    @SerializedName("results")
    val results: List<UserItemResponse>,
    @SerializedName("info")
    val name: UserInfoResponse
) {

    override fun toString(): String {
        return "UserResponse(results=$results, name=$name)"
    }
    
}